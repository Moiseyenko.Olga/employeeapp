﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EmpoyeesApp.Migrations
{
    public partial class nullableFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Birhtday",
                table: "Employees",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PostId",
                table: "Employees",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Birhtday",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "Employees");

        }
    }
}
