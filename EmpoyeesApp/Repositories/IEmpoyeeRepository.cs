﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmpoyeesApp.Models.EmployeeModel;

namespace EmpoyeesApp.Repositories
{
    public interface IEmpoyeeRepository
    {
        IEnumerable<Employee> GetEmployees();
        Employee GetEmployee(int id);
        bool AddEmployee(Employee employee);
        bool ModifyEmployee(Employee employee);
        bool DeleteEmployee(int id);
        IEnumerable<Post> GetPosts();
        List<DiagramExtension> GetStatisticBySex();
        List<DiagramExtension> GetStatisticByYear();
    }
}
