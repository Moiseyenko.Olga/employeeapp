﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using EmpoyeesApp.Models.EmployeeModel;
using EmpoyeesApp.Models;

namespace EmpoyeesApp.Repositories
{
    public class EmployeeRepository: IEmpoyeeRepository
    {
        private readonly EmployeeContext db;

        public EmployeeRepository(EmployeeContext ctx)
        {
            db = ctx;
        }

        public IEnumerable<Employee> GetEmployees()
        {
            IEnumerable<Employee> employees = db.Employees.Include(o => o.Post).AsEnumerable();

            return employees;
        }

        public Employee GetEmployee(int id)
        {
            Employee employee = db.Employees.FirstOrDefault(o => o.Id == id);

            return employee;
        }

        public bool AddEmployee(Employee employee)
        {
            bool result = true;

            try
            {
                var Employees = new Employee()
                {
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    MiddleName = employee.MiddleName,
                    Birhtday = employee.Birhtday,
                    PostId = employee.PostId,
                    Sex = employee.Sex,
                    PhoneNumber = employee.PhoneNumber
                };

                db.Employees.Add(Employees);
                db.SaveChanges();
                return result;
            }
            catch
            {
                return false;
            }
        }

        public bool ModifyEmployee(Employee employee)
        {
            bool result = true;

            try
            {
                var e = db.Employees.FirstOrDefault(o => o.Id == employee.Id);
                
                e.FirstName = employee.FirstName;
                e.LastName = employee.LastName;
                e.MiddleName = employee.MiddleName;
                e.Birhtday = employee.Birhtday;
                e.PostId = employee.PostId;
                e.Sex = employee.Sex;
                e.PhoneNumber = employee.PhoneNumber;

                db.SaveChanges();
                return result;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployee(int id)
        {
            bool result = true;

            try
            {
                var e = db.Employees.FirstOrDefault(o => o.Id == id);
                db.Employees.Remove(e);
                db.SaveChanges();
                return result;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Post> GetPosts()
        {
            IEnumerable<Post> posts = db.Posts.AsEnumerable();

            return posts;
        }

        public List<DiagramExtension> GetStatisticBySex()
        {
            var data = db.Employees
                        .GroupBy(o => o.Sex)
                        .Select(g => new DiagramExtension {
                                Label = g.Key,
                                Value = g.Count()
                        }).ToList();
            return data;
        }

        public List<DiagramExtension> GetStatisticByYear()
        {
            var ranges = new[] { 1940, 1950, 1960, 1970, 1980, 1990, 2000, 2010 };
            var data = db.Employees.GroupBy(x => ranges.FirstOrDefault(r => r > x.Birhtday.Year))
                     .Select(g => new DiagramExtension {
                             Label =((g.Key - 10).ToString() + '-' + (g.Key - 1).ToString()),
                             Value = g.Count() })
                     .OrderBy(g => g.Label)
                     .ToList();
            return data;
        }
    }
}
