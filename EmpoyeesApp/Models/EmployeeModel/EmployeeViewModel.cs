﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmpoyeesApp.Models.EmployeeModel
{
    public class EmployeeViewModel
    {
        public Employee Employee { get; set; } 
        public List<Post> Posts { get; set; }
    }
}
