﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmpoyeesApp.Models.EmployeeModel
{
    public class EmployeeDiagrams
    {
        public List<DiagramExtension> EmployeesBySex { get; set; }
        public List<DiagramExtension> EmployeesByYears { get; set; }
    }

    public class DiagramExtension
    {
        public int Value { get; set; }
        public string Label { get; set; }
    }
}
