﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EmpoyeesApp.Models.EmployeeModel
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Sex { get; set; }
        public DateTime Birhtday { get; set; }
        public int? PostId { get; set; }
        public Post Post { get; set; }
        public string PhoneNumber { get; set; }
    }
}
