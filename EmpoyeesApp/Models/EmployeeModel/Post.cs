﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmpoyeesApp.Models.EmployeeModel
{
    public class Post
    {
        public Post()
        {
            Employee = new HashSet<Employee>();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Employee> Employee { get; set; }
    }
}
