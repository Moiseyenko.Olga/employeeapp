﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmpoyeesApp.Repositories;
using EmpoyeesApp.Models.EmployeeModel;
using Microsoft.AspNetCore.Authorization;

namespace EmpoyeesApp.Controllers
{
    [Authorize(Roles = "Admin, User")]
    public class EmployeeController : Controller
    {
        private readonly IEmpoyeeRepository db;

        public EmployeeController(IEmpoyeeRepository context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Employees");
        }

        public IActionResult Employees()
        {
            IEnumerable<Employee> model = db.GetEmployees().AsEnumerable();

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult NewEmployee()
        {
            var employee = new EmployeeViewModel();
            employee.Posts = db.GetPosts().ToList();
            return View(employee);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult NewEmployee(EmployeeViewModel model)
        {
            db.AddEmployee(model.Employee);
            return RedirectToAction("Employees");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult ModifyEmployee(int id)
        {
            var employee = new EmployeeViewModel();
            employee.Employee = db.GetEmployee(id);
            employee.Posts = db.GetPosts().ToList();
            return View(employee);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult ModifyEmployee(EmployeeViewModel model)
        {
            db.ModifyEmployee(model.Employee);
            return RedirectToAction("Employees");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DeleteEmployee(int id)
        {
            db.DeleteEmployee(id);
            return RedirectToAction("Employees");
        }

        public ActionResult Diagrams()
        {
            var DiagramData = new EmployeeDiagrams();
            DiagramData.EmployeesBySex = db.GetStatisticBySex();
            DiagramData.EmployeesByYears = db.GetStatisticByYear();

            return View(DiagramData);
        }
    }
}