﻿// Write your JavaScript code.
$(document).ready(function () {
    $(".input-cyrilic").keyup(function () {
        this.value = this.value.replace(/[^а-яё]/i, "");
    });

    $(".phone").inputmask({ "mask": "+7 (999) 999-99-99" });

});